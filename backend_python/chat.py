# from flask import Flask, request, jsonify
# import openai

# app = Flask(__name__)

# openai.api_key = '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'

# # Mémoire tampon pour stocker les messages
# message_buffer = []

# def generate_response(user_input):
#     completions = openai.Completion.create(
#         engine='text-davinci-003',
#         prompt=user_input,
#         max_tokens=1024,
#         n=1,
#         stop=None,
#         temperature=0.5,
#     )
#     message = completions.choices[0].text
#     return message

# @app.route('/api/chatbot', methods=['POST'])
# def chatbot():
#     data = request.get_json()
#     user_input = data['user_input']
#     bot_response = generate_response(user_input)
    
#     # Ajout du message de l'utilisateur et de la réponse du bot à la mémoire tampon
#     message_buffer.append({'sender': 'user', 'text': user_input})
#     message_buffer.append({'sender': 'bot', 'text': bot_response})
    
#     return jsonify({'response': bot_response})

# @app.route('/api/messages', methods=['GET'])
# def get_messages():
#     # Retourne la mémoire tampon des messages
#     return jsonify({'messages': message_buffer})

# if __name__ == '__main__':
#     app.run(debug=True)


from PyPDF2 import PdfReader
from langchain.embeddings.openai import OpenAIEmbeddings
from langchain.text_splitter import CharacterTextSplitter
from langchain.vectorstores import elastic_vector_search,pinecone,weaviate,faiss
from langchain.chains.question_answering import load_qa_chain
from langchain.llms import openai
import os
os.environ['OPENAI_API_KEY'] = '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'
# chemin_pdf = r"C:\Users\henri\Documents\cour RCW\prj_rcw_python\politique.pdf"
chemin_pdf=r"C:\Users\henri\Documents\cour RCW\prj_rcw_python\maladie_prevention.pdf"
def extract_pdf():
    with open(chemin_pdf, "rb") as fichier_pdf:
        reader=PdfReader(fichier_pdf)
        raw_text=''
        for i,page in enumerate(reader.pages):
            text=page.extract_text()
            if text:
                raw_text+=text
    splitter = CharacterTextSplitter(
        separator="\n",
        chunk_size=1000,
        chunk_overlap=200,
        length_function=len
    )
    chunks = splitter.split_text(raw_text)
    return chunks

def create_vectorstore():
    chunks=extract_pdf()
    current_embedding = OpenAIEmbeddings()
    vectorestore = faiss.from_texts(texts=chunks, embedding=current_embedding)
    return vectorestore

def generate(input_user):
    chain=load_qa_chain(openai(),chain_type="stuff")
    docsearch=create_vectorstore()
    docs=docsearch.similarity_search(input_user)
    response=chain.run(input_documents=docs,question=input_user)
    return response
extract_pdf()