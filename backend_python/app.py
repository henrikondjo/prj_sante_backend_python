from flask import Flask, jsonify, request
from controllers import create,login,update_patient,ajouter_glucose,get_glucose_data
from classification import classification
from PyPDF2 import PdfReader
from langchain.embeddings.openai import OpenAIEmbeddings
from langchain.text_splitter import CharacterTextSplitter
from langchain.vectorstores import elastic_vector_search,pinecone,weaviate,FAISS
from langchain.chains.question_answering import load_qa_chain
from langchain.chat_models import ChatOpenAI
import os
from flask_cors import CORS  # Importez CORS depuis flask_cors
app = Flask(__name__)
CORS(app)  # Activez CORS pour votre application Flask


os.environ['OPENAI_API_KEY'] = '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'
chemin_pdf = r"C:\Users\henri\Documents\cour RCW\projet_final_rcw\prj_rcw_python\maladie_prevention.pdf"
def extract_pdf():
    with open(chemin_pdf, "rb") as fichier_pdf:
        reader=PdfReader(fichier_pdf)
        raw_text=''
        for i,page in enumerate(reader.pages):
            text=page.extract_text()
            if text:
                raw_text+=text

    splitter = CharacterTextSplitter(
        separator="\n",
        chunk_size=1000,
        chunk_overlap=200,
        length_function=len
    )
    chunks = splitter.split_text(raw_text)
    return chunks

def create_vectorstore():
    chunks=extract_pdf()
    current_embedding = OpenAIEmbeddings()
    vectorestore = FAISS.from_texts(texts=chunks, embedding=current_embedding)
    return vectorestore

def generate(input_user):
    chain=load_qa_chain(ChatOpenAI(),chain_type="stuff")
    docsearch=create_vectorstore()
    docs=docsearch.similarity_search(input_user)
    response=chain.run(input_documents=docs,question=input_user)
    return response


@app.route('/api/chatbot', methods=['POST'])
def chatbot():
    data = request.get_json()
    user_input = data['user_input']
    output = generate(user_input)
    return jsonify({'response': output})

@app.route('/api/register', methods=['POST'])
def register():
    data = request.get_json()
    res=create(data)
    if res:
        response = {'success':'False'}
    else:
        response = {'success':'True'}
    return jsonify(response)

@app.route('/api/login', methods=['POST'])
def Login():
    data = request.get_json()
    user,res=login(data)
    if res:
        response = {'success':'True','data':user}
    else:
        response = {'success':'False','data':None}
    return jsonify(response)



@app.route('/api/modif', methods=['POST'])
def Modif():
    data = request.get_json()
    print(data)
    res=update_patient(data)
    if res:
        response = {'success':'True','msg':'Informations mises à jour avec succès'}
    else:
        response = {'success':'False','msg':'Impossible de faire la mise à jour avec succès'}

    return jsonify(response)



@app.route('/api/glucose', methods=['POST'])
def glucose():
    data = request.get_json()
    print(data)
    res=ajouter_glucose(data)
    if res:
        response = {'success':'True','msg':'Informations de glucose à jour avec succès'}
    else:
        response = {'success':'False','msg':'Impossible  glucose'}

    return jsonify(response)



@app.route('/api/glucose_data', methods=['POST'])
def get_glucose_patient():
    data = request.get_json()
    print(data)
    res,val=get_glucose_data(data)
    if res:
        response = {'success':'True','msg':val}
        print(val)
    else:
        response = {'success':'False','msg':'No data'}
    return jsonify(response)


@app.route('/api/prediction', methods=['POST'])
def predict_patient():
    data = request.get_json()
    print(data)
    resultat=classification(data)
    response = {'success':'True','data':resultat}
    return jsonify(response)











if __name__ == "__main__":
    app.run(debug=True)

